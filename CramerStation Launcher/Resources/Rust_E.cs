﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using MahApps.Metro.Controls;

namespace CramerStation_Launcher
{
    class Rust_E
    {
        
        /// <summary>
        /// Функция для проверки онлайн ли сервер и если да то возврата количества игроков и спящих.
        /// </summary>
        /// <param name="sender">Окно в котором должно всплыть сообщение об ошибке (Окно где мы вызываем данную функцию).</param>
        /// <param name="start">Начальная строка к которой будет добавляться все остальные данные про сервер.</param>
        /// <param name="server_ip">IP игрового сервера.</param>
        /// <param name="server_port">Порт игрового сервера.</param>
        public static async Task<Tuple<List<string>, bool>> Get_Status_Of_server(MetroWindow sender, string server_ip, int server_port)
        {
            try
            {
                List<string> status_of_server = (await NetworkWork.GET_request(sender, "http://" + server_ip + ":" + server_port.ToString() + "/status.json", false)).Split('\r').ToList();

                if (status_of_server[0] == "Error")
                    return Tuple.Create(new List<string> { "0/0", "0" }, false);

                //Елементы массива
                //0 - Игроков онлайн/Максимум игроков
                //1 - Количество спящих
                List<string> list_tmp = new List<string>();
                string str = string.Empty;

                //Ищем количество игроков и сразу убираем лишние
                str = status_of_server.Find(x => x.Contains("players")).Remove(0, 14) + "/" + status_of_server.Find(x => x.Contains("maxplayers")).Remove(0, 17);
                str = str.Replace(",", string.Empty);
                list_tmp.Add(str);

                //Ищем количество спящих игроков и сразу убираем лишние
                str = status_of_server.Find(x => x.Contains("sleepers")).Remove(0, 15);
                str = str.Replace(",", string.Empty);
                list_tmp.Add(str);

                return Tuple.Create(list_tmp, true);
            }
            catch (Exception ex)
            {
                await sender.Dispatcher.Invoke(async () =>
                {
                    await FrontendFunctions.Show_MahApps_Message(sender, 1, "Ошибка", ex.Message + "\n" + ex);
                });
                return Tuple.Create(new List<string> { "Error" }, false);
            }
        }
    }
}
