﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Threading.Tasks;

using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;

namespace CramerStation_Launcher
{
    class FrontendFunctions
    {
        /// <summary>
        /// Функция для вывода сообщения пользователю (замена MessageBox'а).
        /// </summary>
        /// <param name="sender">Окно в котором должно всплыть сообщение</param>
        /// <param name="type">1 - просто сообщение; 2 - сообщение с ввыдом данных</param>
        /// <param name="title">Заголовок сообщения</param>
        /// <param name="text">Текст сообщения</param>
        public static async Task<string> Show_MahApps_Message(MetroWindow sender, int type, string title, string text)
        {
            switch (type)
            {
                case 1:

                    await sender.Dispatcher.Invoke(async () => { await sender.ShowMessageAsync(title, text); });
                    return string.Empty;
                case 2:
                    string str = string.Empty;
                    await sender.Dispatcher.Invoke(async () => { str = await sender.ShowInputAsync(title, text); });
                    return str;
                default:
                    return string.Empty;
            }
        }

        /// <summary>
        /// Функция для включение или отключение активности ProgressRing.
        /// </summary>
        /// <param name="sender">Окно в котором должно всплыть сообщение об ошибке (Окно где мы вызываем данную функцию).</param>
        /// <param name="progress_ring">ProgressRing который должен включить или выключиться.</param>
        /// <param name="type">1 - включить; 0 - выключить.</param>
        /// <param name="count">Количество работаюших ProgressRing.</param>
        public static int Control_Progress_Ring(MetroWindow sender, ProgressRing progress_ring, int type, int count)
        {
            switch (type)
            {
                case 1:
                    sender.Dispatcher.BeginInvoke((Action)delegate () { progress_ring.IsActive = true; });
                    return ++count;
                case 0:
                    if (count == 1)
                        sender.Dispatcher.BeginInvoke((Action)delegate () { progress_ring.IsActive = false; });
                    return --count;
                default:
                    return count;
            }
        }

        /// <summary>
        /// Функция для генерации списка серверов.
        /// </summary>
        /// <param name="sender">Окно в котором должно всплыть сообщение об ошибке (Окно где мы вызываем данную функцию).</param>
        /// <param name="list_box">ListBox в который будут добавляться элементы.</param>
        /// <param name="list_of_servers">Список серверов.</param>
        public async static void Generate_ListBox_Items(MetroWindow sender, ListBox list_box, List<Rust_Servers> list_of_servers)
        {
            try
            {
                for (int i = 0; i < list_of_servers.Count; i++)
                {
                    Grid grid = new Grid();
                    TextBlock textblock = new TextBlock();
                    Image image = new Image();


                    //Замена символов
                    // ' ' - '_'
                    // '#' - '_99_'
                    grid.Name = list_of_servers[i].name
                        .Replace(" ", "_")
                        .Replace("#", "_99_");

                    textblock.Text = list_of_servers[i].name;
                    textblock.VerticalAlignment = VerticalAlignment.Center;
                    textblock.FontSize = 13.333;

                    RenderOptions.SetBitmapScalingMode(image, BitmapScalingMode.HighQuality);
                    image.Name = grid.Name + "_Image";
                    image.Height = 20.0;
                    image.Width = 20.0;
                    image.Margin = new Thickness(173.0, 0.0, 2.0, 0.0);
                    image.Source = new BitmapImage(new Uri(@"pack://application:,,,/Resources/Pictures/offline.png"));

                    grid.Children.Add(textblock);
                    grid.Children.Add(image);

                    list_box.Items.Add(grid);
                }
            }
            catch (Exception ex)
            {
                await Show_MahApps_Message(sender, 1, "Ошибка", ex.Message + "\n" + ex);
            }
        }

        /// <summary>
        /// Finds a Child of a given item in the visual tree. 
        /// </summary>
        /// <param name="parent">A direct parent of the queried item.</param>
        /// <typeparam name="T">The type of the queried item.</typeparam>
        /// <param name="childName">x:Name or Name of child. </param>
        /// <returns>The first parent item that matches the submitted type parameter. 
        /// If not matching item can be found, 
        /// a null parent is being returned.</returns>
        public static T Find_Child<T>(DependencyObject parent, string childName)
           where T : DependencyObject
        {
            // Confirm parent and childName are valid. 
            if (parent == null) return null;

            T foundChild = null;

            int childrenCount = VisualTreeHelper.GetChildrenCount(parent);
            for (int i = 0; i < childrenCount; i++)
            {
                var child = VisualTreeHelper.GetChild(parent, i);
                // If the child is not of the request child type child
                T childType = child as T;
                if (childType == null)
                {
                    // recursively drill down the tree
                    foundChild = Find_Child<T>(child, childName);

                    // If the child is found, break so we do not overwrite the found child. 
                    if (foundChild != null) break;
                }
                else if (!string.IsNullOrEmpty(childName))
                {
                    var frameworkElement = child as FrameworkElement;
                    // If the child's name is set for search
                    if (frameworkElement != null && frameworkElement.Name == childName)
                    {
                        // if the child's name is of the request name
                        foundChild = (T)child;
                        break;
                    }
                }
                else
                {
                    // child element found.
                    foundChild = (T)child;
                    break;
                }
            }

            return foundChild;
        }

        /// <summary>
        /// Функция для отключение элементов интерфейса.
        /// </summary>
        /// <param name="sender">Окно в котором должно всплыть сообщение об ошибке (Окно где мы вызываем данную функцию).</param>
        /// <param name="type">1 - включить; 0 - выключить.</param>
        /// <param name="objects">Массив объектов.</param>
        public static void On_Off_Array_Of_Objects(MetroWindow sender, int type, List<UIElement> objects)
        {
            switch (type)
            {
                case 1:
                    for (int i = 0; i < objects.Count - 1; i++)
                    {
                        sender.Dispatcher.BeginInvoke((Action)delegate () { objects[i].IsEnabled = true; });
                    }
                    
                    break;
                case 0:
                    for (int i = 0; i < objects.Count - 1; i++)
                    {
                        sender.Dispatcher.BeginInvoke((Action)delegate () { objects[i].IsEnabled = false; });
                    }
                        
                    break;
            }
        }

        /// <summary>
        /// Функция для того что бы вывести статус сервера.
        /// </summary>
        /// <param name="sender">Окно в котором должно всплыть сообщение об ошибке (Окно где мы вызываем данную функцию)</param>
        /// <param name="server_ip">IP игрового сервера.</param>
        /// <param name="server_port">Порт игрового сервера.</param>
        /// <param name="reset_status">Сбрасивать ли статус сервера (все просто обнуляеться).</param>
        /// <param name="change_image">Менять ли картинку.</param>
        /// <param name="change_text">Менять текст в статусе.</param>
        /// <param name="image">Картинка которая показует статус сервера.</param>
        /// <param name="text_block">Текст блок там где написан статус сервера.</param>
        public static async void Show_Status_Of_Server(MetroWindow sender, string server_ip, int server_port, bool reset_status, bool change_image, bool change_text, Image image = null, TextBlock text_block = null)
        {
            try
            {
                if (reset_status)
                {
                    await sender.Dispatcher.BeginInvoke((Action)delegate ()
                    {
                        if (change_text)
                        {
                            text_block.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#FF222222"));
                            text_block.Text = "Статус: -\n";
                            text_block.Text += "Игроков онлайн: 0/0\n";
                            text_block.Text += "Спящих: 0";
                        }

                        if (change_image)
                            image.Source = new BitmapImage(new Uri(@"pack://application:,,,/Resources/Pictures/offline.png"));
                    });
                }

                Tuple<List<string>, bool> tuple = await Rust_E.Get_Status_Of_server(sender, server_ip, server_port);

                await sender.Dispatcher.BeginInvoke((Action)delegate ()
                {
                    if (tuple.Item2)
                    {
                        if (change_text)
                        {
                            text_block.Inlines.Clear();
                            text_block.Inlines.Add(new Run("Статус: Онлайн\n") { Foreground = Brushes.GreenYellow });
                            text_block.Inlines.Add(new Run("Игроков онлайн: " + tuple.Item1[0] + "\n"));
                            text_block.Inlines.Add(new Run("Спящих: " + tuple.Item1[1]));
                        }

                        if (change_image)
                            image.Source = new BitmapImage(new Uri(@"pack://application:,,,/Resources/Pictures/online.png"));
                    }
                    else
                    {
                        if (change_text)
                        {
                            text_block.Inlines.Clear();
                            text_block.Inlines.Add(new Run("Статус: Оффлайн\n") { Foreground = Brushes.GreenYellow });
                            text_block.Inlines.Add(new Run("Игроков онлайн: " + tuple.Item1[0] + "\n"));
                            text_block.Inlines.Add(new Run("Спящих: " + tuple.Item1[1]));
                        }

                        if (change_image)
                            image.Source = new BitmapImage(new Uri(@"pack://application:,,,/Resources/Pictures/offline.png"));
                    }
                });
            }
            catch (Exception ex)
            {
                await Show_MahApps_Message(sender, 1, "Ошибка", ex.Message + "\n" + ex);
            }
        }
    }
}
