﻿using System;

namespace CramerStation_Launcher
{
    public static class Paths
    {
        //Путь к папке transmission
        public static string path_to_tm = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\CramerStation Launcher\\transmission";
        //Путь к exe transmission
        public static string path_to_tm_exe = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\CramerStation Launcher\\transmission\\transmission-";
        //Путь к конфигу transmission
        public static string path_to_tm_с = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\CramerStation Launcher\\transmission\\config";
        //IP + Port для локального демона трансмиссии
        public static string ip_and_port_for_tm = "127.0.0.1:9092";
    }

    public class Rust_Servers
    {
        public string name;
        public string server_ip;
        public int server_port;
    }
}
