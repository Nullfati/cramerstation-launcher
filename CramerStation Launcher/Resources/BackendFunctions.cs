﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Diagnostics;
using System.ComponentModel;

using MahApps.Metro.Controls;
using Ionic.Zip;

namespace CramerStation_Launcher
{
    class BackendFunctions
    {
        /// <summary>
        /// Функция для чтения и изменения строки в файле.
        /// </summary>
        /// <param name="sender">Окно в котором должно всплыть сообщение об ошибке (Окно где мы вызываем данную функцию)</param>
        /// <param name="file_path">Путь к файлу</param>
        /// <param name="type">1 - для изменения указанной строки; 2 - для поиска строки</param>
        /// <param name="search">Часть строки которую надо найти</param>
        /// <param name="replace">Строка которая заменить найденую строку</param>
        public static async Task<string> Working_With_File(MetroWindow sender, string file_path, int type, string search, string replace = "")
        {
            try
            {
                string str = string.Empty;
                List<string> list = new List<string>();

                //Путь задается отностельно тобиш без изменений
                //Аккуратно не надо ставить два слеша хотя по идеи должно рабоать и с двумя
                using (System.IO.StreamReader reader = System.IO.File.OpenText(@file_path))
                {
                    str = reader.ReadToEnd();
                }
                list = str.Split('\n').ToList();
                str = string.Empty;


                switch (type)
                {
                    case 1:
                        list[list.FindIndex(x => x.Contains(search))] = replace;

                        using (System.IO.StreamWriter file = new System.IO.StreamWriter(@file_path))
                        {
                            file.Write(string.Join("\n", list));
                        }

                        return "Rewrite done";
                    case 2:
                        return list.Find(x => x.Contains(search));
                    default:
                        return string.Empty;
                }
            }
            catch (Exception ex)
            {
                await sender.Dispatcher.Invoke(async () =>
                {
                    switch (ex.Message)
                    {
                        case "Не удается найти указанный файл":
                            await FrontendFunctions.Show_MahApps_Message(sender, 1, "Ошибка", ex.Message + ":\n" + @file_path);
                            break;
                        default:
                            await FrontendFunctions.Show_MahApps_Message(sender, 1, "Ошибка", ex.Message + "\n" + ex);
                            break;
                    }
                });
                return "Error";
            }
        }

        /// <summary>
        /// Функция для разархивированния архива формата zip.
        /// </summary>
        /// <param name="sender">Окно в котором должно всплыть сообщение об ошибке (Окно где мы вызываем данную функцию)</param>
        /// <param name="overwrite">0 - не перезаписывать файлы; 1 - перезаписывать файлы; 2 - пропускать</param>
        /// <param name="path_to_zip">Путь к архиву</param>
        /// <param name="path_to_unzip">Путь куда разархивировать архив</param>
        public static async void Un_Zip(MetroWindow sender, int overwrite, string path_to_zip, string path_to_unzip)
        {
            try
            {
                //string path = System.IO.Path.GetTempPath() + "..\\" + ((AssemblyCompanyAttribute)Attribute.GetCustomAttribute(Assembly.GetExecutingAssembly(), typeof(AssemblyCompanyAttribute), false)).Company.Replace(" ", "_") + "\\";

                //if (Directory.Exists(path + "CKkozrgUkCgp9Mug8FOe"))
                //    Directory.Delete(path + "CKkozrgUkCgp9Mug8FOe", true);

                //using (ZipFile zip = ZipFile.Read(Assembly.GetExecutingAssembly().GetManifestResourceStream("Arma_3_Launcher_by_HC.Resources.Deluge.CKkozrgUkCgp9Mug8FOe.zip")))
                //{
                //    foreach (ZipEntry tmp in zip)
                //    {
                //        tmp.Extract(path, ExtractExistingFileAction.OverwriteSilently);
                //    }
                //}

                using (ZipFile zip = ZipFile.Read(path_to_zip))
                {
                    foreach (ZipEntry tmp in zip)
                    {
                        switch (overwrite)
                        {
                            case 0:
                                tmp.Extract(path_to_unzip, ExtractExistingFileAction.DoNotOverwrite);
                                break;
                            case 1:
                                tmp.Extract(path_to_unzip, ExtractExistingFileAction.OverwriteSilently);
                                break;
                            case 2:
                                tmp.Extract(path_to_unzip, ExtractExistingFileAction.Throw);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                await sender.Dispatcher.Invoke(async () =>
                {
                    await FrontendFunctions.Show_MahApps_Message(sender, 1, "Ошибка", ex.Message + "\n" + ex);
                });
            }

        }

        public static async void Create_mklink(MetroWindow sender)
        {
            int count = 0;
            const int ERROR_CANCELLED = 1223;

            ProcessStartInfo info = new ProcessStartInfo(@"C:\Windows\System32\cmd.exe");
            info.UseShellExecute = true;
            info.Verb = "runas";

            m1:
            try
            {
                    Process.Start(info);
            }
            catch (Win32Exception ex)
            {
                if (ex.NativeErrorCode == ERROR_CANCELLED)
                {
                    if(count < 5)
                    {
                        await sender.Dispatcher.Invoke(async () =>
                        {
                            await FrontendFunctions.Show_MahApps_Message(sender, 1, "Ошибка", "Необходимо предоставить права администратора. Без этого программа не продолжит роботу.");
                        });
                        count++;
                        goto m1;
                    }
                    else
                    {
                        await sender.Dispatcher.Invoke(async () =>
                        {
                            await FrontendFunctions.Show_MahApps_Message(sender, 1, "Черт тебя дери", "Да нажми ты уже эту чертовую кнопку \"Да\".");
                        });
                        goto m1;
                    }
                    
                }
                else
                {
                    await sender.Dispatcher.Invoke(async () =>
                    {
                        await FrontendFunctions.Show_MahApps_Message(sender, 1, "Ошибка", ex.Message + "\n" + ex);
                    });
                }
            }
            catch (Exception ex)
            {
                await sender.Dispatcher.Invoke(async () =>
                {
                    await FrontendFunctions.Show_MahApps_Message(sender, 1, "Ошибка", ex.Message + "\n" + ex);
                });
            }
        }
    }
}