﻿using System;
using System.IO;
using System.Threading.Tasks;
using System.Net;

using MahApps.Metro.Controls;

namespace CramerStation_Launcher
{
    class NetworkWork
    {
        /// <summary>
        /// GET запрос для html страниц.
        /// </summary>
        /// <param name="sender">Окно в котором должно всплыть сообщение об ошибке (Окно где мы вызываем данную функцию)</param>
        /// <param name="uri">Ссылка на сайт</param>
        /// <param name="show_error">Выводить ли сообщение об ошибке</param>
        public static async Task<string> GET_request(MetroWindow sender, string uri, bool show_error)
        {
            try
            {
                HttpWebRequest http = (HttpWebRequest)WebRequest.Create(uri);
                http.Timeout = 30000;


                Stream stream = http.GetResponse().GetResponseStream();
                var sr = new StreamReader(stream);

                return sr.ReadToEnd();
            }
            catch (Exception ex)
            {
                if (show_error == true)
                {
                    await sender.Dispatcher.Invoke(async () =>
                    {
                        await FrontendFunctions.Show_MahApps_Message(sender, 1, "Ошибка", ex.Message + "\n" + ex);
                    });
                }

                return "Error";
            }
        }
    }
}
