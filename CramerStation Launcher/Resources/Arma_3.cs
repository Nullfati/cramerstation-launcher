﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Net;
using System.Linq;

using BattleNET;
using MahApps.Metro.Controls;

namespace CramerStation_Launcher
{
    class Arma_3
    {
        public static async Task<Tuple<List<string>, bool>> Get_Status_Of_server(MetroWindow sender, string server_ip, int server_port)
        {
            try
            {
                //BattlEyeLoginCredentials blc = new BattlEyeLoginCredentials();
                //blc.Host = Dns.GetHostAddresses(server_ip)[0];
                //blc.Port = server_port;
                //blc.Password = " ";

                //BattlEyeClient bc = new BattlEyeClient(blc);

                //string tmp = bc.Connect().ToString();

                //if (bc.Connect().ToString() != "InvalidLogin")
                //    return Tuple.Create(new List<string> { "0/0" }, false);

                //List<string> number_and_list_of_players = (await NetworkWork.GET_request(sender, "http://cramerstation.ru/servers/" + "ural_server_66_rhs" + ".html", true)).Split(new string[] { "<p>", "</p>", "<br />" }, StringSplitOptions.None).OfType<String>().ToList();
                //number_and_list_of_players.RemoveAt(0);
                //number_and_list_of_players.RemoveAt(number_and_list_of_players.Count - 1);
                //number_and_list_of_players.RemoveAt(1);
                //number_and_list_of_players.RemoveAt(1);
                //number_and_list_of_players.RemoveAt(2);

                ////Получаем количество игроков + максимальное количество игроков
                //number_and_list_of_players[0] = number_and_list_of_players[0].Replace("Текущее количество игроков: ", string.Empty);

                //number_and_list_of_players.RemoveAt(1);

                //return Tuple.Create(number_and_list_of_players, true);
                return null;
            }
            catch (Exception ex)
            {
                await sender.Dispatcher.Invoke(async () =>
                {
                    await FrontendFunctions.Show_MahApps_Message(sender, 1, "Ошибка", ex.Message + "\n" + ex);
                });
                return Tuple.Create(new List<string> { "Error" }, false);
            }
        }
    }
}
