﻿using System;
using System.Threading;
using System.Diagnostics;

using MahApps.Metro.Controls;

namespace CramerStation_Launcher
{
    class TransmissionControl
    {
        /// <summary>
        /// Функция для запуска transmission_daemon.
        /// </summary>
        /// <param name="sender">Окно в котором должно всплыть сообщение об ошибке (Окно где мы вызываем данную функцию)</param>
        public static async void Start_Daemon(MetroWindow sender)
        {
            try
            {
                Process process = new Process();
                process.StartInfo.FileName = Paths.path_to_tm_exe + "daemon-cs.exe";

                process.StartInfo.Arguments = @"-f -g """ + @Paths.path_to_tm_с + @"""";
                process.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                process.StartInfo.UseShellExecute = false;
                process.StartInfo.CreateNoWindow = true;
                process.Start();

                Thread thread = new Thread(() => Auto_Restart_Daemon(sender, process));
                //thread.SetApartmentState(ApartmentState.STA);
                thread.IsBackground = true;
                thread.Start();
            }
            catch (Exception ex)
            {
                await sender.Dispatcher.Invoke(async () =>
                {
                    await FrontendFunctions.Show_MahApps_Message(sender, 1, "Ошибка", ex.Message + "\n" + ex);
                });
            }
        }

        /// <summary>
        /// Функция для автоперезапуска демона трансмиссии при краше.
        /// </summary>
        /// <param name="sender">Окно в котором должно всплыть сообщение об ошибке (Окно где мы вызываем данную функцию)</param>
        /// <param name="process">Процесс transmission-daemon-cs.exe.</param>
        private static async void Auto_Restart_Daemon(MetroWindow sender, Process process)
        {
            try
            {
                while (true)
                {
                    if (!process.Responding || process.HasExited)
                    {
                        process.Kill();
                        Thread.Sleep(5000);
                        process.Start();
                    }

                    Thread.Sleep(30000);
                }
            }
            catch (Exception ex)
            {
                await sender.Dispatcher.Invoke(async () =>
                {
                    await FrontendFunctions.Show_MahApps_Message(sender, 1, "Ошибка", ex.Message + "\n" + ex);
                });
            }
        }

        /// <summary>
        /// Функция для автоперезапуска демона трансмиссии при краше.
        /// </summary>
        /// <param name="sender">Окно в котором должно всплыть сообщение об ошибке (Окно где мы вызываем данную функцию)</param>
        /// <param name="process">Процесс transmission-daemon-cs.exe.</param>
        public static async void Add_Torrent(MetroWindow sender, string path_to_torrent, string path_to_save = null)
        {
            try
            {

            }
            catch (Exception ex)
            {
                await sender.Dispatcher.Invoke(async () =>
                {
                    await FrontendFunctions.Show_MahApps_Message(sender, 1, "Ошибка", ex.Message + "\n" + ex);
                });
            }
        }

        /// <summary>
        /// Функция для автоперезапуска демона трансмиссии при краше.
        /// </summary>
        /// <param name="sender">Окно в котором должно всплыть сообщение об ошибке (Окно где мы вызываем данную функцию)</param>
        /// <param name="process">Процесс transmission-daemon-cs.exe.</param>
        public static async void Close_Daemon(MetroWindow sender)
        {
            try
            {
                Process process = new Process();
                process.StartInfo.FileName = Paths.path_to_tm_exe + "remote-cs.exe";

                process.StartInfo.Arguments = Paths.ip_and_port_for_tm + @" --exit";
                process.StartInfo.RedirectStandardOutput = true;
                process.StartInfo.RedirectStandardError = true;
                //process.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                process.StartInfo.UseShellExecute = false;
                //process.StartInfo.CreateNoWindow = true;
                process.Start();

                //Thread.Sleep(2000);

                //if (!process.HasExited)
                //    process.CloseMainWindow();

                
                string error = process.StandardError.ReadToEnd();
                string output = process.StandardOutput.ReadToEnd();

                if(error != null)
                {

                }
            }
            catch (Exception ex)
            {
                await sender.Dispatcher.Invoke(async () =>
                {
                    await FrontendFunctions.Show_MahApps_Message(sender, 1, "Ошибка", ex.Message + "\n" + ex);
                });
            }
        }
    }
}
