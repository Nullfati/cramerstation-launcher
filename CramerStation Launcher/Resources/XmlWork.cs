﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

using MahApps.Metro.Controls;

namespace CramerStation_Launcher
{
    class XmlWork
    {
        /// <summary>
        /// Функция для создание XML файла где будут храниться настройки.
        /// </summary>
        /// <param name="sender">Окно в котором должно всплыть сообщение об ошибке (Окно где мы вызываем данную функцию)</param>
        /// <param name="path_to_XML">Путь к файлу</param>
        public static async Task<string> Create_XML_File(MetroWindow sender, string path_to_XML)
        {
            try
            {
                XmlTextWriter textWritter = new XmlTextWriter(path_to_XML, Encoding.UTF8);
                textWritter.WriteStartDocument();
                textWritter.WriteStartElement("settings");
                textWritter.WriteEndElement();
                textWritter.Close();

                return "Done";
            }
            catch (Exception ex)
            {
                await sender.Dispatcher.Invoke(async () =>
                {
                    await FrontendFunctions.Show_MahApps_Message(sender, 1, "Ошибка", ex.Message + "\n" + ex);
                });
                return "Error";
            }
        }

        /// <summary>
        /// Функция для добавление записи в XML файл.
        /// </summary>
        /// <param name="sender">Окно в котором должно всплыть сообщение об ошибке (Окно где мы вызываем данную функцию).</param>
        /// <param name="path_to_XML">Путь к файлу</param>
        /// <param name="setting_name">Требуется для добавление записи. Указывает название настойки.</param>
        /// <param name="sub_elements_name">Требуется для добавление записи. Указывает название параметров которые будут записи в настройку.</param>
        /// <param name="sub_elements_parametr">Требуется для добавление записи. Указывает значение параметров.</param>
        public static async Task<string> Add_Node_To_XML(MetroWindow sender, string path_to_XML, string setting_name, List<string> sub_elements_name, List<string> sub_elements_parametr)
        {
            try
            {
                XmlDocument document = new XmlDocument();

                //load from file
                document.Load(path_to_XML);

                //create node and add value
                XmlElement node = document.CreateElement("setting");
                node.SetAttribute("name", setting_name);

                for (int i = 0; i < sub_elements_name.Count; i++)
                {
                    XmlNode subElement = document.CreateElement(sub_elements_name[i]); // даём имя
                    subElement.InnerText = sub_elements_parametr[i]; // и значение
                    node.AppendChild(subElement); // и указываем кому принадлежит
                }

                //add to elements collection
                document.DocumentElement.AppendChild(node);

                //save back
                document.Save(path_to_XML);

                return "Done";
            }
            catch (Exception ex)
            {
                await sender.Dispatcher.Invoke(async () =>
                {
                    await FrontendFunctions.Show_MahApps_Message(sender, 1, "Ошибка", ex.Message + "\n" + ex);
                });
                return "Error";
            }
        }

        /// <summary>
        /// Функция для поиска записи в XML файле.
        /// </summary>
        /// <param name="sender">Окно в котором должно всплыть сообщение об ошибке (Окно где мы вызываем данную функцию)</param>
        /// <param name="type">1 - создать файл; 2 - добавляем запись; 3 - найти указанные данные</param>
        /// <param name="path_to_XML">Путь к файлу</param>
        /// <param name="setting_name">Требуется для добавление записи. Указывает название настойки.</param>
        /// <param name="sub_elements_name">Требуется для добавление записи. Указывает название параметров которые будут записи в настройку.</param>
        /// <param name="sub_elements_parametr">Требуется для добавление записи. Указывает значение параметров.</param>
        public static async Task<List<string>> Find_Node_In_XML_File(MetroWindow sender, string path_to_XML, string find, List<string> find_value)
        {
            try
            {
                List<string> list_str = new List<string>();
                XmlDocument document = new XmlDocument();

                document.Load(path_to_XML);

                XmlNodeList nodes = document.SelectNodes("/parameters/setting[@name='" + find + "']");


                for (int i = 0; i < find_value.Count; i++)
                {
                    foreach (XmlNode node in nodes)
                    {
                        XmlNode xn = node.SelectSingleNode(find_value[i]);
                        if (xn != null)
                            list_str.Add(xn.InnerText);
                    }
                }

                return list_str;
            }
            catch (Exception ex)
            {
                await sender.Dispatcher.Invoke(async () =>
                {
                    await FrontendFunctions.Show_MahApps_Message(sender, 1, "Ошибка", ex.Message + "\n" + ex);
                });
                return new List<string> { "Error" };
            }
        }
    }
}
