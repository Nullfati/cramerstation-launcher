﻿using System.Windows;

using MahApps.Metro.Controls;

namespace CramerStation_Launcher
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        public MainWindow()
        {
            InitializeComponent();
            this.AllowsTransparency = true;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            
        }

        private void Close_Click(object sender, RoutedEventArgs e)
        {

            Application.Current.Shutdown();
        }

        private void test_Click(object sender, RoutedEventArgs e)
        {
            TransmissionControl.Close_Daemon(this);
        }
    }
}
